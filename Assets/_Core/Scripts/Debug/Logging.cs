﻿using UnityEngine;

public class Logging : MonoBehaviour
{
    [SerializeField] bool isOn = true;
    public static Logging instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(this);
        }
    }

    public void Debugger(string _message)
    {
        if (this.isOn)
        {
            Debug.Log($"-> {_message}");
        }
    }
}
