﻿using System.Collections;
using UnityEngine;

public class Drawing : MonoBehaviour
{
    public static Drawing instance;
    [SerializeField] bool isOn = true;
    [SerializeField] float time = 5f;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(this);
        }
    }

    public void DrawBall(Vector3 _position)
    {
        this.DrawBall(_position, Color.yellow, 1f);
    }

    public void DrawBall(Vector3 _position, Color _color, float _size)
    {
        if (this.isOn)
        {
            GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            Renderer renderer = sphere.GetComponent<Renderer>();
            Material material = sphere.GetComponent<Material>();

            sphere.transform.position = _position;
            sphere.transform.localScale = new Vector3(_size, _size, _size);
            sphere.layer = 2;

            renderer.material = new Material(Shader.Find("Specular"));
            renderer.material.SetColor("_Color", _color);

            StartCoroutine(DestroyAfter(sphere));
        }
    }

    public void DrawVerticalRay(Vector3 _newPosition)
    {
        this.DrawVerticalRay(_newPosition, 100f, Color.green);
    }

    public void DrawVerticalRay(Vector3 _newPosition, float _height, Color _color)
    {
        if (this.isOn)
        {
            Vector3 verticalRay = _newPosition;
            verticalRay.y = _height;

            Debug.DrawRay(verticalRay, new Vector3(0f, -100f, 0f), _color, this.time);
        }
    }

    public void DrawRay(Vector3 _newPosition, Vector3 _direction)
    {
        this.DrawRay(_newPosition, _direction, 1f, Color.red);
    }

    public void DrawRay(Vector3 _newPosition, Vector3 _direction, float _depth, Color _color)
    {
        if (this.isOn)
        {
            Vector3.Normalize(_direction);
            Debug.DrawRay(_newPosition, _direction * _depth, _color, this.time);
        }
    }

    // * Internals

    private IEnumerator DestroyAfter(GameObject _gameObject)
    {
        yield return new WaitForSeconds(this.time);
        Destroy(_gameObject);
    }
}
