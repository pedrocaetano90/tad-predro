using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{

    public float camSpeed = 0.1f;

    void Start()
    {
        
    }

    void Update()
    {
        // Move
        if(Input.GetKey("w") ) {
            this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z + this.camSpeed);
        } else if(Input.GetKey("s") ) {
            this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z - this.camSpeed);
        }

        if(Input.GetKey("a") ) {
            this.transform.position = new Vector3(this.transform.position.x - this.camSpeed, this.transform.position.y, this.transform.position.z);
        } else if(Input.GetKey("d") ) {
            this.transform.position = new Vector3(this.transform.position.x + this.camSpeed, this.transform.position.y, this.transform.position.z);
        }

        //         // Zoom
        // float scroll = Input.GetAxis("Mouse ScrollWheel");
        // if(scroll != 0) {
        //     // TODO: speed increase with height
        //     if(scroll > 0) {
        //         this.transform.Translate(scroll * Vector3.forward * this.panSpeed * this.zoomSpeed * frameDistanceFactor * accelerationfactor);
        //         if(this.transform.position.y < this.limitBottom) {
        //             this.transform.position = new Vector3(transform.position.x, this.limitBottom, transform.position.z);
        //         }
        //     } 
            
        //     if (scroll < 0) {
        //         this.transform.Translate(scroll * Vector3.forward * this.panSpeed * this.zoomSpeed * frameDistanceFactor * accelerationfactor);
        //     }
        // }
    }
}
