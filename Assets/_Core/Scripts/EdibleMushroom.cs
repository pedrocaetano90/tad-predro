using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EdibleMushroom : MonoBehaviour
{
    public GameObject parentObject;
    public float spawnChance = 0.9f;
    public float maturingTime = 5f;
    public float spawnInterval = 1.0f;
    public float maturity = 0.0f; // 0 - 1.0f

    private float growthRate = 1f;

    void Start()
    {
        InvokeRepeating ("TrySpawn", this.maturingTime, this.spawnInterval);
    }

    public void Initialize(int neighbours) {
        this.maturity = 0.0f;
        this.transform.localScale = Vector3.zero;
        this.growthRate =  Mathf.Min (2 / Mathf.Sqrt (neighbours), 1);
    }

    void Update()
    {
        float growth = Time.deltaTime / this.maturingTime * this.growthRate;
        this.maturity = Mathf.Min (this.maturity + growth, 1.5f *  this.growthRate);

        this.transform.localScale = new Vector3(maturity, maturity, maturity);
    }

    private void TrySpawn() {
        if (this.maturity < 1) return;
        float diceRoll = Random.Range(0, 1.0f);
        if (diceRoll > this.spawnChance) return;

        float randomAngle = Random.Range(0, 360) * Mathf.Deg2Rad;
        float randomDist = Random.Range(1, 30);
        Vector3 newPos = (Vector3.forward * Mathf.Cos(randomAngle) + Vector3.right * Mathf.Sin(randomAngle)) * randomDist + this.transform.position;
            
        Vector3? maybeGroundPoint = GetGroundPosition(newPos);

        if(maybeGroundPoint is Vector3 groundPoint){
            int mushroomsMask = LayerMask.GetMask("Mushrooms");
            int neighbours = Physics.OverlapSphere(groundPoint, 20f, mushroomsMask).Length;
            if (neighbours > 3) return;

            int treesMask = LayerMask.GetMask("Trees");
            bool isOnATree = Physics.OverlapSphere(groundPoint, 0.5f, treesMask).Length > 0;
            bool isNearATree = Physics.OverlapSphere(groundPoint, 3f, treesMask).Length > 0;
            
            if (isOnATree) return;
            if (!isNearATree) return;

            GameObject child = Instantiate(this.gameObject, groundPoint, Quaternion.identity, parentObject.transform);
            child.GetComponent<EdibleMushroom>().Initialize(neighbours);
        }
    }

    private Vector3? GetGroundPosition(Vector3 newPos) {
        RaycastHit hit;
        Vector3 rayDirection = new Vector3(0, -1, 0);
        Vector3 rayOrigin = new Vector3(newPos.x, newPos.y + 50, newPos.z);

        Drawing.instance.DrawRay(rayOrigin, rayDirection, 100f, Color.red);

        int waterLayer = LayerMask.NameToLayer("Water");
        int groundLayer = LayerMask.NameToLayer("Ground");

        int mask = LayerMask.GetMask("Ground", "Water", "Rocks");
        if (Physics.Raycast (rayOrigin, rayDirection, out hit, 100f, mask)) {
            if (hit.transform.gameObject.layer == groundLayer) return hit.point;
            return null;
        }

        return null;
    }
}
