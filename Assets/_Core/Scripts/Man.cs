using System.Collections;
using System.Collections.Generic;
using UnityEngine.AI;
using UnityEngine;

public class Man : MonoBehaviour
{
    public float nutrition = 1f;
    public float stamina = 1f;

    public GameObject? target = null;

    public NavMeshAgent  navMeshAgent;
    public Animator animator;


    public ManState state = ManState.idle;

    public enum ManState
    {
        idle,
        gather
    }

    void Start()
    {
        InvokeRepeating ("GetHungry", 0, 1f);
        this.animator.SetTrigger("IsIdle");
    }

    void Update()
    {
        switch(state) {
            default:
            case ManState.idle:
                IdleUpdate();
                break;
            case ManState.gather:
                if (this.target is GameObject target)
                    GatherUpdate(target);
                else 
                    this.state = ManState.idle;
                break;

        }
    }

    private void IdleUpdate() {
        if(this.nutrition < 1.0f && Sun.IsDaytime()) {
            int mushroomsMask = LayerMask.GetMask("Mushrooms");
            Collider[] mushroomsColliders = Physics.OverlapSphere(this.transform.position, 200f, mushroomsMask);
            if (mushroomsColliders[0] is Collider mushroomCollider) {
                this.target = mushroomCollider.gameObject;
                this.state = ManState.gather;
                this.navMeshAgent.isStopped = false;
                this.navMeshAgent.SetDestination(this.target.transform.position);
                this.animator.SetTrigger("IsWalking");
            }
        }
    }

    private void GatherUpdate(GameObject target) {
        if (! Sun.IsDaytime()) {
            this.state = ManState.idle;
            this.target = null;
            this.animator.SetTrigger("IsIdle");
            this.navMeshAgent.isStopped = true;
        }

        int mushroomsMask = LayerMask.GetMask("Mushrooms");
        Collider[] mushroomsColliders = Physics.OverlapSphere(this.transform.position, 1f, mushroomsMask);
        if(mushroomsColliders.Length == 0) return;
        if (mushroomsColliders[0] is Collider mushroomCollider) {
            this.state = ManState.idle;
            this.target = null;
            this.animator.SetTrigger("IsIdle");
            this.navMeshAgent.isStopped = true;
            Destroy(mushroomCollider.gameObject);
            this.nutrition += 0.5f;
        }
    }

    private void GetHungry() {
        int stateMultiplier = 1;
        switch (this.state)
        {
            case ManState.gather:
                stateMultiplier = 2;
                break;
        }
        float decrease = (Sun.TimeOfDay() + 0.5f) % 1 / (Sun.dayDuration * this.stamina);
        this.nutrition -= decrease * stateMultiplier;

        if (this.nutrition < 0) Die();
    }

    private void Die() {
        Destroy(this.gameObject);
    }
}

