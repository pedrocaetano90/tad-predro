using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class Sun : MonoBehaviour
{
    public Light myLight;

    public static bool IsDaytime () {
        if (Sun.TimeOfDay () > 0.8) return false;
        if (Sun.TimeOfDay () < 0.2) return false;
        return true;
    }

    public static float dayDuration = 4f * 60f;
    public static float TimeOfDay () {
        return ((Time.time + dayDuration / 4) / Sun.dayDuration % 1);
    }

    void Start()
    {
        
    }

    void Update()
    {
        // Debug.Log(Sun.TimeOfDay());
        this.transform.rotation = Quaternion.Euler((360 * Sun.TimeOfDay()) - 90, 0, 0);

        float intensity = Mathf.Max (1f - (Mathf.Abs(Sun.TimeOfDay() - 0.5f) * 3.5f), 0);

        myLight.intensity = intensity;
    }
}
