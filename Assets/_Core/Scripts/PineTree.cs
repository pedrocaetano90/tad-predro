using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PineTree : MonoBehaviour
{
    public GameObject parentObject;
    // public GameObject treePrefab;
    public float spawnChance = 0.05f;
    public float maturingTime = 10f;
    public float spawnInterval = 1.0f;
    public float maturity = 0.0f; // 0 - 1.0f

    private float growthRate = 1f;

    void Start()
    {
        InvokeRepeating ("TrySpawn", this.maturingTime, this.spawnInterval);
    }

    public void Initialize(int neighbours) {
        this.maturity = 0.0f;
        this.transform.localScale = Vector3.zero;
        this.growthRate =  Mathf.Min (2 / Mathf.Sqrt (neighbours), 1);
    }

    void Update()
    {
        float growth = Time.deltaTime / this.maturingTime * this.growthRate;
        this.maturity = Mathf.Min (this.maturity + growth, 1.5f *  this.growthRate);

        this.transform.localScale = new Vector3(maturity, maturity, maturity);
    }

    private void TrySpawn() {
        if (this.maturity < 1) return;
        float diceRoll = Random.Range(0, 1.0f);
        if (diceRoll > this.spawnChance) return;

        float randomAngle = Random.Range(0, 360) * Mathf.Deg2Rad;
        float randomDist = Random.Range(5, 10);
        Vector3 newPos = (Vector3.forward * Mathf.Cos(randomAngle) + Vector3.right * Mathf.Sin(randomAngle)) * randomDist + this.transform.position;
            
        Vector3? maybeGroundPoint = GetGroundPosition(newPos);

        if(maybeGroundPoint is Vector3 groundPoint){
            int layerMask = LayerMask.GetMask("Trees");
            int neighbours = Physics.OverlapSphere(groundPoint, 5f, layerMask).Length;

            if (neighbours > 20) return;

            GameObject newTreeInstance = Instantiate(this.gameObject, groundPoint, Quaternion.identity);
            newTreeInstance.transform.SetParent(parentObject.transform);
            newTreeInstance.GetComponent<PineTree>().Initialize(neighbours);
        }

    }

    private Vector3? GetGroundPosition(Vector3 newPos) {
        RaycastHit hit;
        Vector3 rayDirection = new Vector3(0, -1, 0);
        Vector3 rayOrigin = new Vector3(newPos.x, newPos.y + 50, newPos.z);

        Drawing.instance.DrawRay(rayOrigin, rayDirection, 100f, Color.red);
        if (Physics.Raycast (rayOrigin, rayDirection, out hit, 100f)) {
            int layerInt = hit.transform.gameObject.layer;

            if(layerInt != 6) return null;
            else return hit.point;
        }

        return null;
    }
}
